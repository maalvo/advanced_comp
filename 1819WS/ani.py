import numpy as np
import matplotlib.pyplot as plt

numBins = 100
numEvents = 100000

file = 'datafile_100bins_100000events.histogram'
histogramSeries = np.fromfile(file, int).reshape(-1,numBins)

fig, ax = plt.subplots()
rects = ax.bar(range(numBins), np.ones(numBins)*40)  # 40 is upper bound of y-axis 

for i in range(numEvents):
    [rect.set_height(h) for rect,h in zip(rects,histogramSeries[i,:])]
    fig.canvas.draw()
    plt.pause(0.001)
